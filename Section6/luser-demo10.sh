#!/bin/sh

readonly PROGRAM="${0##*/}"
readonly VERBOSE='true'

log() {
	# Sends a message to syslog and to standard output if VERBOSE is true.
	_message="$*"
	[ "$VERBOSE" = 'true' ] && printf '%s\n' "$_message"
	logger -t "$PROGRAM" "$_message"
}

backup_file() {
	# Creates backup of a given file.
	_files="$*"
	for _file in $_files; do
		if [ -f "$_file" ]; then
			_backup_file="/var/tmp/${_file##*/}.$(date +%s)"
			log "Backing up $_file to $_backup_file."
			cp -p "$_file" "$_backup_file" || return "$?"
		fi
	done
}


log 'Hello!'
log 'This is fun!'

if [ backup_file ]; then
	log 'File backup succeeded'
else
	log 'File backup failed!'
fi
exit 0
