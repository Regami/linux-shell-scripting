#!/bin/sh
# This script generates a random password.
# The user can set hte password length with -l and add a special character with -s
# Verbose mode can be enabled with -v.

readonly PROGRAM="${0##*/}"
readonly SPECIAL_CHARACTERS="!@#$%^&*()-+="

# Set a default password length
LENGTH=48
VERBOSE='false'
SPECIAL_CHAR='false'

usage() {
	while IFS= read -r _line; do
		printf '%s\n' "$_line"
	done <<-EOF
	Usage: $PROGRAM [-vs ] [-l LENGTH]
	$PROGRAM generate a random password.
	
	   -l LENGTH         Specify the password length
	   -s                Append a special character to the password
	   -v                Increase verbosity
	EOF
}

say() {
	_msg="$*"
	[ "$VERBOSE" = 'true' ] && printf '%s: %s\n' "$PROGRAM" "$_msg"
}

while getopts "vl:s" OPTION; do
	case "$OPTION" in
		v)	VERBOSE='true'; printf 'Verbose mode is on.\n' ;;
		l)	LENGTH="$OPTARG" ;;
		s)	SPECIAL_CHAR='true' ;;
		*)	usage; exit 1 ;;
	esac
done

say 'Generating a password.'
PASSWORD="$(date +%s%N | sha256sum | head -c "$LENGTH")"
if [ "$SPECIAL_CHAR" = 'true' ]; then
	say 'Selecting a random special character.'
	_random_special="$(printf '%s' "$SPECIAL_CHARACTERS" | fold -w 1 | shuf | head -c 1)"
	PASSWORD="${PASSWORD}$_random_special"
fi

say 'Done'
printf "%s\n" "$PASSWORD"

exit 0
