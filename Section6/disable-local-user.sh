#!/bin/sh
# 

readonly PROGRAM="${0##*/}"
readonly ARCHIVE_DIR='/archive'
USERS=''

# Defaults
REMOVE_ACCOUNT='false'
REMOVE_OPTION='-r'
ARCHIVE_HOME='false'

print_error() {
	printf '%s: Error %s.\n' "$PROGRAM" "$@"
}

die() {
	print_error "$@"
	exit 1
}

check_privileges() {
	[ "$(id -u)" -eq 0 ] || die 'must be executed by root'
}

usage() {
	while IFS= read -r _line; do
		printf '%s\n' "$_line"
	done <<-EOF
	Usage: $PROGRAM [ -dra ] USER [USERN]...
	$PROGRAM disable user accounts.
	
	   -d                Delete user instead of disable
	   -r                Remove associated home directory
	   -a                Create an archive of the homedir
	                     defaults to '/archive'
	EOF
}

check_user_existance() {
	_begin_uids=1000
	for user in $USERS; do
		_uid=$(id -u "$user") || die "user '$user' does not exist"
		if [ "$_uid" -lt "$_begin_uids" ]; then
			print_error "user '$user' has id $_uid"
			die "can not perform actions on system accounts i.e. UIDs less than $_begin_uids"
		fi
	done
}

archive_home() {
	[ -d "$ARCHIVE_DIR" ] || mkdir "$ARCHIVE_DIR"
	for user in $USERS; do
		_user_home=$(getent passwd "$user" | cut -d : -f 6)
		if [ ! -d "$_user_home" ]; then
			print_error "can not archive '$_user_home' directory does not exist"
			continue
		fi
		tar -zcf "$ARCHIVE_DIR/${user}_$(date +%s).tar.gz" "$_user_home" > /dev/null 2>&1
	done
}

disable_account() {
	for user in $USERS; do
		chage -E 0 "$user"
	done
}

delete_account() {
	for user in $USERS; do
		userdel "$REMOVE_OPTION" "$user" > /dev/null 2>&1
	done
}

main() {
	while getopts "dra" OPTION; do
		case "$OPTION" in
			d)	REMOVE_ACCOUNT='true' ;;
			r)	REMOVE_OPTION='-r' ;;
			a)	ARCHIVE_HOME='true' ;;
			*)	usage; exit 1 ;;
		esac
	done
	shift "$(( OPTIND - 1))"
	[ "${#}" -lt 1 ] && (usage; exit 1)
	USERS="$*"
	check_user_existance
	[ "$ARCHIVE_HOME" = 'true' ] && archive_home
	if [ "$REMOVE_ACCOUNT" = 'true' ]; then
		delete_account
	else
		disable_account
	fi
}

check_privileges
main "$@"
exit 0
