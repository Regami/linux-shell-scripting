#!/bin/sh
# This script deletes a user.

PROGRAM="${0##*/}"

die() {
	printf '%s: Error %s.\n' "$PROGRAM" "$@"
	exit 1
}

check_privileges() {
	[ "$(id -u)" -eq 0 ] || die 'must be executed by root'
}

main() {
	check_privileges
	_user="$1"
	[ -z "$_user" ] && die 'USER is a mandatory argument'
	userdel "$_user" || die "user '$_user' is NOT deleted"
}

main "$@"
exit 0
