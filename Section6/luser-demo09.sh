#!/bin/sh
# This script demonstrates the case statement.

#if [ "$1" = 'start' ]; then
#	printf 'Starting.\n'
#elif [ "$1" = 'stop' ]; then
#	printf 'Stop.\n'
#elif [ "$1" = 'status' ]; then
#	printf 'Status:\n'
#else
#	printf 'Supply a valid option.\n' >&2
#	exit 1
#fi

case "$1" in
	start)				printf 'Starting.\n' ;;
	stop)				printf 'Stopping.\n' ;;
	status|state|--status|--state)	printf 'Status:\n' ;;
	*)				printf 'Supply a valid option.\n' >&2; exit 1
esac
