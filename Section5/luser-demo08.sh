#!/bin/sh
# This scripts demonstrates I/O redirection.

# Redirect stdout to a file.
FILE="/tmp/$(basename -s .sh "$0")"
head -n 1 /etc/passwd > "$FILE"

# Redirect stdin to a program.
read LINE < "$FILE"
printf '%s\n' "$LINE"

# Redirect stdout to a file, overwriting the file.
head -n 3 /etc/passwd > "$FILE"
printf '\nContents of %s\n' "$FILE"
cat "$FILE"

# Redirect stdout to a file, appending to a file.
head -c 32 /dev/random | base64 >> "$FILE"
echo
cat "$FILE"
