#!/bin/sh
# Add local user accounts.


PROGRAM="${0##*/}"

die() {
	printf '%s: Error %s.\n' "$PROGRAM" "$@"
	exit 1
}

check_privileges() {
	[ "$(id -u)" -eq 0 ] || die 'must be executed by root'
}

check_parameters() {
	[ -n "$1" ] || return
	[ -n "$2" ] || return
}

print_usage() {
	printf 'Usage: %s LOGIN COMMENT\n' "$PROGRAM"
	exit 1
}

generate_password() {
	head -c 32 /dev/random | base64
}

main() {
	check_privileges
	check_parameters "$@" || print_usage
	useradd -c "$2" -m "$1" || die 'account was not able to be created'
	password=$(generate_password)
	printf '%s:%s' "$1" "$password" | chpasswd
	passwd -e "$1" 1>/dev/null
	printf "User '%s' with password '%s' was created on '%s'\n" "$1" "$password" "$(cat /etc/hostname)"
}

main "$@"
exit 0
