#!/bin/sh
# This script creates an account on the local system.
# You will be prompted for the account name and password

# Ask for the user name.
read -p 'Enter the username to create: ' USER_NAME

# Ask for the real name.
read -p 'Enter ther name of the person who this account is for: ' COMMENT

# Create the user.
useradd -c "$COMMENT" -m "$USER_NAME"

# Set the password for the user.
passwd "$USER_NAME"

# Force password change on first login.
passwd -e "$USER_NAME"
