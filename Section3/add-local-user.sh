#!/bin/sh
# Add local user accounts.


PROGRAM="${0##*/}"

#
# BEGIN helper functions
#

die() {
	printf '%s: Error %s.\n' "$PROGRAM" "$@"
	exit 1
}

check_privileges() {
	[ "$(id -u)" -eq 0 ] || die 'must be executed by root'
}

#
# END helper functions
#

main() {
	check_privileges
	printf 'Enter login name: '; read -r login
	printf 'Enter username: '; read -r comment
	useradd -c "$comment" -m "$login" || die 'account was not able to be created'
	passwd "$login" || die 'password could not be set'
	passwd -e "$login"
	printf "User %s was created on %s\n" "$login" "$(cat /etc/hostname)"
}

main "$@"
exit 0
