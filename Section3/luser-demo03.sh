#!/bin/sh
# Display the UID and username of the user executing this script.
# Display if the user is the root user or not.

# Display the UID
USER_ID=$(id -u)
printf 'Your UID is %s\n' "$USER_ID"

# Only display if the UID does not match 1000.
UID_TO_TEST_FOR=1000
if [ "$USER_ID" -ne "$UID_TO_TEST_FOR" ]; then
	printf 'Your UID does not match %s\n' "$UID_TO_TEST_FOR" >&2
	exit 1
fi

# Display the username
USER_NAME=$(id -un)

# Test if the command succeeded
if [ "$?" -ne 0 ]; then
	printf 'The id command did not execute succesfully.'
	exit 1
fi
printf "Your username is %s\n" "$USER_NAME"

# You can use a string test conditional.
USER_NAME_TO_TEST_FOR='wybren'
[ "$USER_NAME" = "$USER_NAME_TO_TEST_FOR" ] && \
	printf 'Your username matches %s\n' "$USER_NAME_TO_TEST_FOR"

# Test for != (not equal) for the string
if [ "$USER_NAME" != "$USER_NAME_TO_TEST_FOR" ]; then
	printf 'Your username does not match %s' "$USER_NAME_TO_TEST_FOR"
	exit 1
fi
