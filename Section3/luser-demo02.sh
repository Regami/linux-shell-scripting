#!/bin/sh
# Display the UID and username of the user executing this script.
# Display if the user is the root user or not.

# Display the UID
USER_ID=$(id -u)
echo "Your UID is $USER_ID"

# Display the username
USER_NAME=$(id -un)
echo "Your username is $USER_NAME"

# Display if the user is the root user or not.
[ "$USER_ID" -eq 0 ] \
	&& echo 'You are root' \
	|| echo 'You are not root'
