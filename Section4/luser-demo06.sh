#!/bin/sh
# This script generates a random password for each user specified on the command line.

# Display what the user typed on the command line.
#printf 'You executed this command: %s\n' "$0"

# Display the path and filename of the script.
#printf 'You used %s as the path of the %s script\n' "$(dirname "$0")" "$(basename "$0")"

# Tell them how mant arguments they have passed in.
#NUMBER_OF_PARAMETERS=$#
#printf 'You have supplied %s argument(s) on the command line.\n' "$NUMBER_OF_PARAMETERS"

# Make sure they at least supply one argument
if [ "$#" -lt 1 ]; then
	printf 'Usage: %s USER_NAME [USER_NAME]...\n' "$(basename "$0")"
	exit 1
fi

for user in "$@"; do
	password=$(head -c 32 /dev/random | base64)
	printf '%s: %s\n' "$user" "$password"
done
