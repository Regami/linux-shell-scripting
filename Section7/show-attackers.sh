#!/bin/sh

PROGRAM="${0##*/}"
LIMIT=10
LOG_FILE="$1"

die() {
	print_err "$*"
	exit 1
}

print_err() {
	_msg="$*"
	printf '%s: Error %s.\n' "$PROGRAM" "$_msg"
}

main() {
	[ ! -e "$LOG_FILE" ] || die "cannot open log file: $LOG_FILE"
	printf 'Count,IP,Location'
	grep Failed "$LOG_FILE" | awk '{print $(NF - 3)}' | sort | uniq -c | sort -nr | while read count ip
	do
		if [ "$count" -gt "$LIMIT" ]; then
			_location=$(geoiplookup "$ip")
			printf "%s,%s,%s" "$count" "$ip" "$_location"
		fi
	done
}

main "$@"
exit 0
