#!/bin/sh
# This script shows the open network ports on a system.

ss -4tupln | grep -Ev '^Netid' | awk '{print $5}' | awk -F ':' '{print $NF}'
