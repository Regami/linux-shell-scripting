#!/bin/sh
# Run same command on multiple systems via SSH

# $ cat ~/.servers
# ape-nl-ee-clr1-103 192.168.0.103
# ape-nl-ee-clr1-106 192.168.0.106

readonly PROGRAM="${0##*/}"
# CONFIG is not readonly because -f FILE can override.
CONFIG="${RUNEVERYWHERE_CONFIG_HOME:-$HOME/.servers}"

# Default values for args
DRYRUN=0
SUDO=0
VERBOSE=0


die() {
	print_err "$@"
	exit 1
}

print_err() {
	_msg="$*"
	printf '%s: Error %s.\n' "$PROGRAM" "$_msg" >&2
}

preflight() {
	[ "$(id -u)" -ge 1000 ] || die "can not executre with root or application user"
}

usage() {
	while IFS= read -r _line; do
		printf '%s\n' "$_line"
	done <<-EOF
	Usage: $PROGRAM [ -f FILE ] [ -nsvh ] <command>
	
	   -f FILE           Override default conmfig location
	   -n                Perform a dry-run (prints commands)
	   -s                Run commands on target with sudo
	   -v                Enable verbose mode
	   -h                Prints this help message
	EOF
}

main() {
	while getopts "f:nsvh" OPTION; do
		case "$OPTION" in
			f)	CONFIG="$OPTARG" ;;
			n)	DRYRUN=1 ;;
			s)	SUDO=1 ;;
			v)	VERBOSE=1 ;;
			h)	usage; exit 0 ;;
			*)	usage; exit 1 ;;
		esac
	done
	[ -e "$CONFIG" ] || die "'$CONFIG' does not exist"
	shift "$(( OPTIND - 1))"
	if [ "$#" -lt 1 ]; then
		usage
		exit 1
	fi
	
	
	aliases="$(cut -d ' ' -f 1 < "$CONFIG")"
	for server in $aliases; do
		_cmd="$*"
		[ "$SUDO" -eq '0' ] || _cmd="sudo $_cmd"
		[ "$VERBOSE" -eq 0 ] || printf '%s:\n' "$server"
		if [ "$DRYRUN" -eq 0 ]; then
			ssh -o ConnectTimeout=2 "$server" "$_cmd"
			exit_status="$?"
			if [ "$exit_status" -ne 0 ]; then
				print_err "'$_cmd' on '$server' failed"
			fi
		else
			printf '%s\n' "$_cmd"
		fi
	done
}

preflight
main "$@"
exit "$exit_status" 
