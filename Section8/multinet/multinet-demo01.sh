#!/bin/sh
# This script pings a list of servers and reports their status.

# $ cat ~/.servers
# ape-nl-ee-clr1-103 192.168.0.103
# ape-nl-ee-clr1-106 192.168.0.106

readonly PROGRAM="${0##*/}"
readonly SERVER_FILE="$HOME/.servers"

die() {
	print_err "$@"
	exit 1
}

print_err() {
	_msg="$*"
	printf '%s: Error %s.\n' "$PROGRAM" "$_msg" >&2
}

[ -e "$SERVER_FILE" ] || die "'$SERVER_FILE' does not exist"
_servers=$(cut -d ' ' -f 2 < ~/.servers)
for server in $_servers; do
	printf 'Pinging: %s\n' "$server"
	[ "$(ping -c 1 "$server" 2>/dev/null)" ] || printf '%s: down\n' "$server" >&2
	printf '%s: up\n' "$server"
done
